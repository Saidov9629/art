<?php

return [



    'Home page' =>  'Home',
    'Contact us'=>  'About Us',
    'Servis'    =>  'Servises',
    'Portfole'  =>  'Portfole',
    'Team'      =>  'Team',
    'Gallery'   =>  'Gallery',
    'Contact'   =>  'Contact',
    'Language'  =>  'EN',
    'Login'     =>  'Login',
    'head'      =>  'Art Gallery',
    'head1'     =>  'Welcome!',
    'h1'        =>  '25',
    'h2'        =>  'Projects',
    'h3'        =>  'Videos',
    'h4'        =>  'Musics',
    'h5'        =>  'Fails',
    'f'         =>  'Uzbekistan, Khorezm <br>Urgench city, Al-beruniy<br>28-home',
    'f2'        =>  '<strong>Phone:</strong> +99 894 123 4566<br> <strong>Email:</strong> info@example.com<br>',
    'f3'        =>  'Useful links',
    'f4'        =>  'Our services',
    'f5'        =>  ' <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Web developer</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Project Manger</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Designer</a></li>',
    'f6'        =>  '<h4>Our news</h4>
                    <p>Our team will keep you informed about new products and news</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>',
    'in'        =>  'Speaking of us. We are engaged in the sale of works of art.',
    'in1'       =>  'To date, we have made great strides. We bring centuries of art from many countries. We also have a museum with a personal exhibition',
    'in2'       =>  '<li><i class="ri-check-double-line"></i> Quality is 100% guaranteed.</li>
                        <li><i class="ri-check-double-line"></i> Original brand copies.</li>
                        <li><i class="ri-check-double-line"></i> Meets world quality standards.</li>',
    'in3'       =>  'We have been operating since 2010. We have more than 100 stores and outlets around the world. We have also arranged for the delivery of orders. We are currently planning to implement new plans.',
    'in4'       =>  '<h4>Fails</h4> <p>You can apply at home</p>',
    'in5'       =>  '<h4>3D pictures</h4> <p>We have 3D pictures, you can easily order</p>',
    'in6'       =>  '<h4>Diversity</h4> <p>Various types of artwork are featured on our website</p>',
    'in7'       =>  '<h4>Quality assurance</h4> <p>Quality is 100% guaranteed. Validity period is 20 years. </p>',
    'in8'       =>  '<h2>Our services</h2> <p>Check out our services</p>',
    'in9'       =>  '<p>Worldwide delivery service available from 5 days to 25 days</p>',
    'in10'      =>  '<h4><a href="">Online registration</a></h4> <p>You can apply at home</p>',
    'in11'      =>  '<h4><a href="">Easy and fast</a></h4>  <p>Find and order easily and quickly. We will deliver to you</p>',
    'in12'      =>  '<h4><a href="">Contacts</a></h4> <p>Connections are available at all outlets around the world</p>',
    'in13'      =>  '<h4><a href="">Check and see</a></h4> <p>You can see information about the product in the video before you receive it</p>',
    'in14'      =>  '<h4><a href="">Museums</a></h4> <p>You can get information about all museums here</p>',
    'in15'      =>  '<h2>Portfolio</h2>  <p>Our Portfolio</p>',
    'in16'      =>  '<h3>Meets world standards</h3> <p>The members of the commission checked our quality and gave a very high rating</p>',
    'in17'      =>  '<p><strong>We are satisfied customers</strong> We are customers who have been cooperating with us</p>',
    'in18'      =>  '<p><strong>Projects</strong> So far, we have developed and launched more than 50 projects</p>',
    'in19'      =>  '<p><strong>Fast and Quality</strong> Our staff provides fast and quality service</p>',
    'in20'      =>  '<p><strong>Wine</strong> We are glad to share our achievements with you</p>',
    'in21'      =>  '<h3>Comments left</h3> <h4>Tokyo &amp; Sartan</h4>',
    'in22'      =>  'I am very happy with the services and products of this company. I think we will do more together in the future.',
    'in23'      =>  'I have achieved a lot in terms of design and I really appreciate the quality of the products of this company..',
    'in24'      =>  '<h3>Jena Karlis</h3>   <h4>Store Owner</h4>',
    'in25'      =>  'I like the art gallery of this company. I took a lot of pictures here.',
    'in26'      =>  '<h3>Matt Brandon</h3>  <h4>Freelancer</h4>',
    'in27'      =>  'I am impressed by the fast and quality delivery. Usually other companies would deliver in around 30 days.',
    'in28'      =>  '<h3>John Larson</h3>   <h4>Entrepreneur</h4>',
    'in29'      =>  'I wish good luck to this company. I really like his services.',
    'in30'      =>  '<h2>Our Team</h2>  <p>Check out our team</p>',
    'in31'      =>  '<h4>Walter White</h4>   <span>CEO</span>',
    'in32'      =>  '<h4>Sarah Jhonson</h4>  <span>Chief Engineer</span>',
    'in33'      =>  '<h4>William Anderson</h4> <span>General Programmer</span>',
    'in34'      =>  '<h4>Amanda Jepson</h4>   <span>Chief Accountant</span>',
    'g1'        =>  'Samples from the gallery',
    'c1'        =>  '<h2>Contact</h2>   <p>Find us easy</p>',
    'c2'        =>  '<h4>Adress:</h4>     <p>25, Urgench, NM 220100</p>',
    'c3'        =>  '<h4>Phone:</h4>',
    'c4'        =>  'Send messages',
    'c5'        =>  'A message has been sent to you. Thank you!',
    'c6'        =>  'Loading',
    'c7'        =>  'Mail',
    'c8'        =>  '<h3>Rightnow contact Us</h3> <p>The staff at our call centers will serve you and answer your questions..</p>
                <a class="cta-btn" href="#">contact us</a>',
];

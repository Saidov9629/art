<?php

return [



    'Home page' =>  'Bosh sahifa',
    'Contact us'=>  'Biz haqmizda',
    'Servis'    =>  'Xizmatlar',
    'Portfole'  =>  'Portfole',
    'Team'      =>  'Jamoamiz',
    'Gallery'   =>  'Galleriya',
    'Contact'   =>  'Boglanish',
    'Language'  =>  'UZ',
    'Login'     =>  'Krish',
    'head'      =>  'Sanat Galleriyamizga',
    'head1'     =>  'Xush kelibsiz!',
    'h1'        =>  '25 ta',
    'h2'        =>  'Loyhalar',
    'h3'        =>  'Videolar',
    'h4'        =>  'Musiqalar',
    'h5'        =>  'Fayillar',
    'f'         =>  'Uzbekistan, Xorazm <br>Urganch shaxri, Al-beruniy<br>28-uy',
    'f2'        =>  '<strong>Tel:</strong> +99 894 123 4566<br> <strong>Email:</strong> info@example.com<br>',
    'f3'        =>  'Foydali linklar',
    'f4'        =>  'Bizning xizmatlar',
    'f5'        =>  ' <li><i class="bx bx-chevron-right"></i> <a href="#">Web Dizayen</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Web Ishlabchiqish</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Maxsulot Menijiri</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Grafik Dizayen</a></li>',
    'f6'        =>  '<h4>Bizning yangliklar</h4>
                    <p>Bizning jamoamiz sizga yangi maxsulotlar va yanglilar haqida xabar berib turadi</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>',
    'in'        =>  'Biz haqmizda aytib otsak. Biz sanat asarlari oldi sotdisi bilan shugulanamiz.',
    'in1'       =>  'Bugunga kelib judda katta yutuqlarga erishdik. Kop davlatlardan sanat asrlari olib kleib sotib kelmoqdamiz. Ozmini shaxsiy korgazmali muzeymiz ham bor',
    'in2'       =>  '<li><i class="ri-check-double-line"></i> Sifati 100% kafolatlanadi.</li>
                        <li><i class="ri-check-double-line"></i> Asil brend nusxalar.</li>
                        <li><i class="ri-check-double-line"></i> Jahon sifat stardartlariga javob beradi.</li>',
    'in3'       =>  '2010-yildan beri faolyat yurutib kelmoqdamiz. Butun dunyo boylab 100 dan ortiq dokon va shahobchalarmiz bor. Buyurtmalarni yetkazib berisni ham yolga qoyganmiz. Hozirda yangi rejalarni amalga oshrishni reja qilmoqdamiz.',
    'in4'       =>  '<h4>Hujjatlar</h4> <p>Uyda turgan holda hujjatlarni rasmiylashtrishinggiz mumnkin</p>',
    'in5'       =>  '<h4>3D rasmlar</h4> <p>Bizda 3D rsmlar bor, bemalol zakaz berishinggiiz mumkin</p>',
    'in6'       =>  '<h4>Turli xillik</h4> <p>Har xil turdagi sanat asarlari bizning web saytmizdan joy eggalagan</p>',
    'in7'       =>  '<h4>Sifat kafolati</h4> <p>Sifat kafolati 100% beriladi. Amal qilishi muddati 20 yil. </p>',
    'in8'       =>  '<h2>Xizmatlarmiz</h2> <p>Bizning xizmatlarni tekshiring</p>',
    'in9'       =>  '<p>Dunyo boylab yetkazib berish xizmati mavjud 5 kundan - 25 kungacha</p>',
    'in10'      =>  '<h4><a href="">Online rasmiylashtrish</a></h4> <p>Uyda turgan holda hujjatlarni rasmiylashtrishinggiz mumnkin </p>',
    'in11'      =>  '<h4><a href="">Oson va tez</a></h4>  <p>Oson va tez toping va buyurtma bering. Biz sizga yetkazib beramiz</p>',
    'in12'      =>  '<h4><a href="">Aloqalar</a></h4> <p>Aloqalar dunyo boylab barcha shaxobchalarda mavjud</p>',
    'in13'      =>  '<h4><a href="">Tekshrish va korish</a></h4> <p>Maxsulotni olishdan oldin u haqda malumotlarni video shakilda korishinggiz mumkin</p>',
    'in14'      =>  '<h4><a href="">Muzeylar</a></h4> <p>Barcha muzeylar haqida shu yerdan malumot olishinggiz mumkin</p>',
    'in15'      =>  '<h2>Portfolio</h2>  <p>Bizning Portfolio</p>',
    'in16'      =>  '<h3>Jaxon standartiga javob beradi</h3> <p>Kommisya azolari bizning sifatmizni tekshrishdi va judda katta baho berishdi</p>',
    'in17'      =>  '<p><strong>Mamnun mijozlarmiz</strong> Biz bilan hamkorlik qilib kelayotgan mijozlarmiz</p>',
    'in18'      =>  '<p><strong>Loyxalarmiz</strong> Hozirga kleib 50 tadan ortiq loyhalarni ishlab chiqdik va ishga tushurdik</p>',
    'in19'      =>  '<p><strong>Tez va Sifatli</strong> Xodimlarmiz tez va sifatli xizmat korsatishadi</p>',
    'in20'      =>  '<p><strong>Yutuqlar</strong> Biz erishgan yutuqlar, siz bilan baham korganmizdan xursandmiz</p>',
    'in21'      =>  '<h3>Qoldrilgan komentaryalar</h3> <h4>Tokyo &amp; Sartan</h4>',
    'in22'      =>  'Men bu konponyani xizmatlari va maxsulotlaridan judda xursandman. Oylaymanki kelajakda bundan ham kop ishlarni hamkorlikda birga qilamiz.',
    'in23'      =>  'Men dizayen boicha judda kop yutuqlarga erishganman va Bu konponyani maxsulotlarni sifatini judda katta baholayman.',
    'in24'      =>  '<h3>Jena Karlis</h3>   <h4>Store Owner</h4>',
    'in25'      =>  'Menga ushbu konponyaning art-galleriyasi yoqadi. Koplab rasmlarni shu yerdan olganman.',
    'in26'      =>  '<h3>Matt Brandon</h3>  <h4>Freelancer</h4>',
    'in27'      =>  'Tez va sifatli yetkazib berishiga qoyil qolganman. Odatda boshqa konponyalar 30 kun atrofida yetkazib berishar edi.',
    'in28'      =>  '<h3>John Larson</h3>   <h4>Entrepreneur</h4>',
    'in29'      =>  'Ushbu konponyaga omad tilayman. Menga xizmatlari judda yoqadi.',
    'in30'      =>  '<h2>Bizning Jamoa</h2>  <p>Bizning jamoani tekshring</p>',
    'in31'      =>  '<h4>Walter White</h4>   <span>Bosh direktor</span>',
    'in32'      =>  '<h4>Sarah Jhonson</h4>  <span>Bosh muxanddis</span>',
    'in33'      =>  '<h4>William Anderson</h4> <span>Bosh Dasturchi</span>',
    'in34'      =>  '<h4>Amanda Jepson</h4>   <span>Bosh hisobchi</span>',
    'g1'        =>  'Galleriyadan namunalar',
    'c1'        =>  '<h2>Boglanish</h2>   <p>Bizni oson toping</p>',
    'c2'        =>  '<h4>Manzil:</h4>     <p>25-uy, Urganch shaxri, NM 220100</p>',
    'c3'        =>  '<h4>Tell:</h4>',
    'c4'        =>  'Xabarni jonatish',
    'c5'        =>  'Sizni xabar jonatildi. Raxmat!',
    'c6'        =>  'Jonatilmoqda',
    'c7'        =>  'Pochta',
    'c8'        =>  '<h3>Hoziroq Biz bilan boglaning</h3> <p>Bizning call-markazlarimiz xodimlari sizga xizmat qiladi va sizning savollaringizga javob beradi..</p>
                <a class="cta-btn" href="#">Boglanish</a>',
];

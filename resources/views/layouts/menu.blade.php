<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Foydalanuvchilar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('teams*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('teams.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Jamoalar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('teamMembers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('teamMembers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Jamoa Azolari</span>
    </a>
</li>


<li class="nav-item {{ Request::is('authors*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('authors.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Avtor</span>
    </a>
</li>
<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Toifa</span>
    </a>
</li>
<li class="nav-item {{ Request::is('art*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('art.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sa'nat</span>
    </a>
</li>

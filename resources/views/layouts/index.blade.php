<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Art-gallery</title>
    <meta content="gallery" name="art-gallery">
    <meta content="gallery" name="gallery">

    <!-- Favicons -->
    <link href="{{asset("assets2/img/favicon.png")}}" rel="icon">
    <link href="{{asset("assets2/img/apple-touch-icon.png")}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset("assets2/vendor/aos/aos.css")}}" rel="stylesheet">
    <link href="{{asset("assets2/vendor/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("assets2/vendor/bootstrap-icons/bootstrap-icons.css")}}" rel="stylesheet">
    <link href="{{asset("assets2/vendor/boxicons/css/boxicons.min.css")}}" rel="stylesheet">
    <link href="{{asset("assets2/vendor/glightbox/css/glightbox.min.css")}}" rel="stylesheet">
    <link href="{{asset("assets2/vendor/remixicon/remixicon.css")}}" rel="stylesheet">
    <link href="{{asset("assets2/vendor/swiper/swiper-bundle.min.css")}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset("assets2/css/style.css")}}" rel="stylesheet">


</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-lg-between">

        <h1 class="logo me-auto me-lg-0"><a href="index.html">A-G<span>.</span></a></h1>

        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a class="nav-link scrollto active" href="{{url('index')}}">{{__('words.Home page')}}</a></li>
                <li><a class="nav-link scrollto" href="#about">{{__('words.Contact us')}}</a></li>
                <li><a class="nav-link scrollto" href="#services">{{__('words.Servis')}}</a></li>
                <li><a class="nav-link scrollto " href="#portfolio">{{__('words.Portfole')}}</a></li>
                <li><a class="nav-link scrollto" href="#team">{{__('words.Team')}}</a></li>
                <li><a class="nav-link scrollto" href="{{url('gallery')}}">{{__('words.Gallery')}}</a></li>
                <li><a class="nav-link scrollto" href="{{url('contact')}}">{{__('words.Contact')}}</a></li>
                <li class="dropdown"><a href=""><span>{{__('words.Language')}}</span> <i class="bi bi-chevron-down"></i></a>
                <ul>
                  <li><a href="{{url('/lang/en')}}">EN</a></li>
                  <li><a href="{{url('/lang/uz')}}">UZ</a></li>
                  <li><a href="{{url('/lang/ru')}}">RU</a></li>
              </li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

        <a href="{{url('login')}}" class="get-started-btn scrollto">{{__('words.Login')}}</a>

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

        <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
            <div class="col-xl-6 col-lg-8">
                <h1>{{__('words.head')}} <span>.</span></h1>
                <h2>{{__('words.head1')}}</h2>
            </div>
        </div>

        <div class="row gy-4 mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
            <div class="col-xl-2 col-md-4">
                <div class="icon-box">
                    <i class="ri-team-line"></i>
                    <h3><a href="">{{__('words.h1')}}</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4">
                <div class="icon-box">
                    <i class="ri-gallery-line"></i>
                    <h3><a href="">{{__('words.h2')}}</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4">
                <div class="icon-box">
                    <i class="ri-video-line"></i>
                    <h3><a href="">{{__('words.h3')}}</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4">
                <div class="icon-box">
                    <i class="ri-music-2-line"></i>
                    <h3><a href="">{{__('words.h4')}}</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4">
                <div class="icon-box">
                    <i class="ri-file-line"></i>
                    <h3><a href="">{{__('words.h5')}}</a></h3>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Hero -->

@yield('content')

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>A-G<span>.</span></h3>
                        <p>{!! __('words.f') !!}
                            <br><br>
                             {!! __('words.f2') !!}
                        </p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>{{__('words.f3')}}</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">{{__('words.Home page')}}</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">{{__('words.Contact us')}}</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">{{__('words.Servis')}}</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">{{__('words.Gallery')}}</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">{{__('words.Portfole')}}</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>{{__('words.f4')}}</h4>
                    <ul>
                       {!! __('words.f5') !!}
                    </ul>
                </div>

                <div class="col-lg-4 col-md-6 footer-newsletter">
                    {!! __('words.f6') !!}

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>Gp</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/ -->
            Designed by <a href="https://bootstrapmade.com/">WebDeveloper</a>
        </div>
    </div>
</footer><!-- End Footer -->

<div id="preloader"></div>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>



@yield('script')

<!-- Vendor JS Files -->
<script src="{{asset("assets2/vendor/aos/aos.js")}}"></script>
<script src="{{asset("assets2/vendor/bootstrap/js/bootstrap.bundle.min.js")}}"></script>
<script src="{{asset("assets2/vendor/glightbox/js/glightbox.min.js")}}"></script>
<script src="{{asset("assets2/vendor/isotope-layout/isotope.pkgd.min.js")}}"></script>
<script src="{{asset("assets2/vendor/php-email-form/validate.js")}}"></script>
<script src="{{asset("assets2/vendor/purecounter/purecounter.js")}}"></script>
<script src="{{asset("assets2/vendor/swiper/swiper-bundle.min.js")}}"></script>

<!-- Template Main JS File -->
<script src="{{asset("assets2/js/main.js")}}"></script>

</body>

</html>


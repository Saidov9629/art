@extends('layouts.app')

@section('content')

    <ol class="breadcrumb">

        <li class="breadcrumb-item active">Dashbord</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info " style="border-radius: 5px">
                        <div class="inner">
                            <h3 class="text-center" >150</h3>

                            <h5 class="text-dark" style="margin-left: 10px">Rasmlar</h5>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Batafsil malumot<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger " style="border-radius: 5px">
                        <div class="inner">
                            <h3 class="text-center" >1563</h3>

                            <h5 class="text-dark" style="margin-left: 10px">Yuklamalar</h5>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Batafsil malumot<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-secondary " style="border-radius: 5px">
                        <div class="inner">
                            <h3 class="text-center" >14</h3>

                            <h5 class="text-dark" style="margin-left: 10px" >Jamoalar</h5>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Batafsil malumot<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                    <!-- small box -->

                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success " style="border-radius: 5px">
                        <div class="inner">
                            <h3 class="text-center" >116</h3>

                            <h5 class="text-dark" style="margin-left: 10px">Foydalanuvchilar</h5>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Batafsil malumot<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>

    </div>
@endsection

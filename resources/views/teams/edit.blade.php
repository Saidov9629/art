@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('teams.index') !!}">Jamoa</a>
          </li>
          <li class="breadcrumb-item active">Taxrirlash</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Jamoani taxrirlash</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($team, ['route' => ['teams.update', $team->id], 'method' => 'patch', 'enctype'=>"multipart/form-data",]) !!}

                              @include('teams.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection

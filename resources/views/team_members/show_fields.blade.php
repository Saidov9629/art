<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $teamMember->id }}</p>
</div>

<!-- Team Id Field -->
<div class="form-group">
    {!! Form::label('team_id', 'Jamoa Id:') !!}
    <p>{{ $teamMember->team_id }}</p>
</div>

<!-- Pasition Field -->
<div class="form-group">
    {!! Form::label('pasition', 'Lavozimi:') !!}
    <p>{{ $teamMember->pasition }}</p>
</div>

<!-- Full Name Field -->
<div class="form-group">
    {!! Form::label('full_name', 'Toliq FIO:') !!}
    <p>{{ $teamMember->full_name }}</p>
</div>

<!-- Info Field -->
<div class="form-group">
    {!! Form::label('info', 'Info:') !!}
    <p>{{ $teamMember->info }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Yaratilgan vaqti:') !!}
    <p>{{ $teamMember->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Yangilangan vaqti At:') !!}
    <p>{{ $teamMember->updated_at }}</p>
</div>


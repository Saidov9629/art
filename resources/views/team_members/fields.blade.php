<!-- Team Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('team_id', 'Team Id:') !!}
    <select class="select" name="team_id" id="team_id">
        @foreach($teams as $team)
            <option value="{{$team->id}}">{{$team->name}}</option>
        @endforeach
    </select>
</div>

<!-- Pasition Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pasition', 'Lavozimi:') !!}
    {!! Form::text('pasition', null, ['class' => 'form-control']) !!}
</div>

<!-- Full Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('full_name', 'Toliq IFO:') !!}
    {!! Form::text('full_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('info', 'Info:') !!}
    {!! Form::text('info', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('teamMembers.index') }}" class="btn btn-secondary">Bekor qilish</a>
</div>

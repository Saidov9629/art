@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('authors.index') }}">Avtor</a>
            </li>
            <li class="breadcrumb-item active">Malumot</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Malumotlar</strong>
                                  <a href="{{ route('authors.index') }}" class="btn btn-light">Bekor qilish</a>
                             </div>
                             <div class="card-body">
                                 @include('authors.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection

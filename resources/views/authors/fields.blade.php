<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Rasm:') !!}
    <input type="file"
           name="image"
           accept="image/png, image/jpeg">
</div>
<div class="clearfix"></div>

<!-- Full Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('full_name', 'Toliq IFO:') !!}
    {!! Form::text('full_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Info Field -->
<div class="form-group col-sm-6">
    {!! Form::label('info', 'Info:') !!}
    {!! Form::text('info', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('authors.index') }}" class="btn btn-secondary">Bekor qilish</a>
</div>

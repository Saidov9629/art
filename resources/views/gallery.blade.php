@extends('layouts.index')

@section('content')

    <main id="main">


        <!-- ======= Features Section ======= -->
        <section id="features" class="features">
            <div class="container" data-aos="fade-up">

                <h2 align="center">{!! __('words.g1') !!}</h2>
                <center><hr style="width: 35%; border: thick; color: black; "></center><br>
                @foreach($team_members as $member)

                <div class="row">
                    <div class="image col-lg-6" style='background-image: url("{{asset("assets2/img/features.jpg")}}");' data-aos="fade-right">
                    </div>
                    <div class="col-lg-6" data-aos="fade-left" data-aos-delay="100">
                        <div class="icon-box mt-5 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
                            <i class="bx bx-receipt"></i>
                            <h4>{!! $member->full_name !!}</h4>
                            <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                        </div>
                        <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
                            <i class="bx bx-cube-alt"></i>
                            <h4>{!! $member->info !!}</h4>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                        </div>
                        <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
                            <i class="bx bx-images"></i>
                            <h4>{!! $member->pasition !!}</h4>
                            <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
                        </div>
                        <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
                            <i class="bx bx-shield"></i>
                            <h4>{!! $member->team_id !!}</h4>
                            <p>Expedita veritatis consequuntur nihil tempore laudantium vitae denat pacta</p>
                        </div>
                    </div>
                </div> <br><hr>
                @endforeach
            </div>
        </section>

        <div class="container">
            <h2 class="header-inner-pages">Arts</h2>
            <hr>
            <div class="row">

                @foreach($arts as $art)
                    <div class="col-md-3">
                        <a class="ripple" href="#!">
                            <img src="{{url('/files/uploaded_arts/'.$art->images)}}"  alt="About" class="img-fluid rounded " >
                            <p class="text-center">{!! $art->tags !!}</p>
                            <b class="text-danger">{!! $art->description[\Illuminate\Support\Facades\App::getLocale()] !!}</b>
                            <h3 class="text-end">{!! $art->title[\Illuminate\Support\Facades\App::getLocale()]!!}</h3><br>
                        </a>
                    </div>
                @endforeach

            </div>
            <br>
            <div>
                <h4 class="alert-heading">Jamoalarmiz</h4>
                <hr>
                <div class="row">
                    @foreach($teams as $team)
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <a href="{{url('/files/uploaded/'.$team->image)}}">
                                <img src="{{url('/files/uploaded/'.$team->image)}}" alt="Lights" style="width:100%">
                                <div class="caption">
                                    <i>{!! $team->name !!}</i>
                                    <p>{!! $team->description !!}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <br>
        </div>
        <br>

    </main><!-- End #main -->

@endsection






@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('categories.index') !!}">Toifa</a>
          </li>
          <li class="breadcrumb-item active">Taxrirlash</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Toifani taxrirlsh</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'patch', 'files' => true]) !!}

                              <!-- Name Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('name', 'Ism:') !!}
                                      {!! Form::text('name_uz', $category->name['uz'], ['class' => 'form-control']) !!}
                                      {!! Form::text('name_ru', $category->name['ru'], ['class' => 'form-control']) !!}
                                      {!! Form::text('name_en', $category->name['en'], ['class' => 'form-control']) !!}
                                  </div>

                                  <!-- Image Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('image', 'Rasm:') !!}
                                      <input type="file"
                                             name="image"
                                             accept="image/png, image/jpeg">
                                  </div>
                                  <div class="clearfix"></div>

                                  <!-- Submit Field -->
                                  <div class="form-group col-sm-12">
                                      {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
                                      <a href="{{ route('categories.index') }}" class="btn btn-secondary">Bekor qilish</a>
                                  </div>


                                  {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection

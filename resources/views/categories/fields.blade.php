<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Ism:') !!}
    {!! Form::text('name_uz', null, ['class' => 'form-control']) !!}<br>
    {!! Form::text('name_ru', null, ['class' => 'form-control']) !!}<br>
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Rasm:') !!}
    <input type="file"
           name="image"
           accept="image/png, image/jpeg">
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categories.index') }}" class="btn btn-secondary">Bekor qilish</a>
</div>

@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('art.index') }}">San'at</a>
            </li>
            <li class="breadcrumb-item active">Malumot</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Malumotlar</strong>
                                  <a href="{{ route('art.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('art.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection

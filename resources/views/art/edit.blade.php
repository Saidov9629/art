@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('art.index') !!}">San'at</a>
          </li>
          <li class="breadcrumb-item active">Taxrirlsh</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>San'atni taxrirlash</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($art, ['route' => ['art.update', $art->id], 'method' => 'patch', 'files' => true]) !!}

                              <!-- Category Id Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('category_id', 'Toifa Id:') !!}
                                      <select class="select" name="category_id" id="category_id">
                                          @foreach($categories as $category)
                                              <option value="{{$category->id}}">{{$category->id}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <!-- Title Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('title', 'Sarlovha:') !!}
                                      {!! Form::text('title_uz', $art->title['uz'], ['class' => 'form-control']) !!}<br>
                                      {!! Form::text('title_ru', $art->title['ru'], ['class' => 'form-control']) !!}<br>
                                      {!! Form::text('title_en', $art->title['en'], ['class' => 'form-control']) !!}
                                  </div>

                                  <!-- Description Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('description', 'Tarif:') !!}
                                      {!! Form::text('description_uz', $art->description['uz'], ['class' => 'form-control']) !!}<br>
                                      {!! Form::text('description_ru', $art->description['ru'], ['class' => 'form-control']) !!}<br>
                                      {!! Form::text('description_en', $art->description['en'], ['class' => 'form-control']) !!}
                                  </div>

                                  <!-- Images Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('images', 'Rasmlar:') !!}
                                      <input type="file"
                                             name="image"
                                             accept="image/png, image/jpeg">
                                  </div>
                                  <div class="clearfix"></div>

                                  <!-- Tags Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('tags', 'Teglar:') !!}
                                      {!! Form::text('tags', $art->tags, ['class' => 'form-control']) !!}
                                  </div>

                                  <!-- Author Id Field -->
                                  <div class="form-group col-sm-6">
                                      {!! Form::label('author_id', 'Avtor Id:') !!}
                                      <select class="select" name="author_id" id="author_id">
                                          @foreach($authors as $author)
                                              <option value="{{$author->id}}">{{$author->full_name}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <!-- Submit Field -->
                                  <div class="form-group col-sm-12">
                                      {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
                                      <a href="{{ route('art.index') }}" class="btn btn-secondary">Bekor qilish</a>
                                  </div>


                                  {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection

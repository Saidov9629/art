<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $art->id }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Toifa Id:') !!}
    <p>{{ $art->category_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Sarlovha:') !!}
    <p>{{ $art->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Tarif:') !!}
    <p>{{ $art->description }}</p>
</div>

<!-- Images Field -->
<div class="form-group">
    {!! Form::label('images', 'Rasmlar:') !!}
    <p>{{ $art->images }}</p>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tags', 'Teglar:') !!}
    <p>{{ $art->tags }}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', 'Avtor Id:') !!}
    <p>{{ $art->author_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Yaratilgan vaqt:') !!}
    <p>{{ $art->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Yangilangan vaqt:') !!}
    <p>{{ $art->updated_at }}</p>
</div>


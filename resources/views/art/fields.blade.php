<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Toifa Id:') !!}
    <select class="select" name="category_id" id="category_id">
        @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->id}}</option>
        @endforeach
    </select>
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Sarlovha:') !!}
    {!! Form::text('title_uz', null, ['class' => 'form-control']) !!}<br>
    {!! Form::text('title_ru', null, ['class' => 'form-control']) !!}<br>
    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Tarif:') !!}
    {!! Form::text('description_uz', null, ['class' => 'form-control']) !!}<br>
    {!! Form::text('description_ru', null, ['class' => 'form-control']) !!}<br>
    {!! Form::text('description_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Images Field -->
<div class="form-group col-sm-6">
    {!! Form::label('images', 'Rasmlar:') !!}
    <input type="file"
           name="image"
           accept="image/png, image/jpeg">
</div>
<div class="clearfix"></div>

<!-- Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tags', 'Teglar:') !!}
    {!! Form::text('tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Author Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('author_id', 'Avtor Id:') !!}
    <select class="select" name="author_id" id="author_id">
        @foreach($authors as $author)
            <option value="{{$author->id}}">{{$author->full_name}}</option>
        @endforeach
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('art.index') }}" class="btn btn-secondary">Bekor qilish</a>
</div>

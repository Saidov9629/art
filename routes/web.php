<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashbordController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

Route::resource('users', App\Http\Controllers\UserController::class);

Route::resource('teams', App\Http\Controllers\TeamController::class);

Route::resource('teamMembers', App\Http\Controllers\Team_memberController::class);

Route::resource('authors', App\Http\Controllers\AuthorController::class);

Route::resource('categories', App\Http\Controllers\CategoryController::class);

Route::resource('art', App\Http\Controllers\ArtController::class);

Route::get('/', function () { return view('home'); });
Route::get('gallery', function () { return view('gallery'); });
Route::get('contact', function () { return view('contact'); });
Route::get('/index',   [\App\Http\Controllers\HomeController::class, 'index']);
Route::get('lang/{locale}',[\App\Http\Controllers\LanguageController::class,'index']);

Route::get('/gallery',    [FrontController::class,'gallery']);

Route::get('/login',     [AuthController::class,'login'])->name('login');
Route::post('/login',     [AuthController::class,'checklogin']);
Route::get('/logout',    [AuthController::class,'logout'])->name('logout');

Route::get('/dashbord', [\App\Http\Controllers\DashbordController::class, 'dashbord']);


<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('teams', App\Http\Controllers\API\TeamAPIController::class);

Route::resource('team_members', App\Http\Controllers\API\Team_memberAPIController::class);

Route::resource('authors', App\Http\Controllers\API\AuthorAPIController::class);

Route::resource('categories', App\Http\Controllers\API\CategoryAPIController::class);

Route::resource('art', App\Http\Controllers\API\ArtAPIController::class);
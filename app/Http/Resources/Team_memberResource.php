<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Team_memberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'team_id' => $this->team_id,
            'pasition' => $this->pasition,
            'full_name' => $this->full_name,
            'info' => $this->info,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}

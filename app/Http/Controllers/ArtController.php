<?php

namespace App\Http\Controllers;

use App\DataTables\ArtDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateArtRequest;
use App\Http\Requests\UpdateArtRequest;
use App\Models\Art;
use App\Models\Author;
use App\Models\Category;
use App\Models\Team;
use App\Repositories\ArtRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ArtController extends AppBaseController
{
    /** @var  ArtRepository */
    private $artRepository;

    public function __construct(ArtRepository $artRepo)
    {
        $this->artRepository = $artRepo;
    }

    /**
     * Display a listing of the Art.
     *
     * @param ArtDataTable $artDataTable
     * @return Response
     */
    public function index(ArtDataTable $artDataTable)
    {
        return $artDataTable->render('art.index');
    }

    /**
     * Show the form for creating a new Art.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();
        $authors = Author::all();

        return view('art.create', compact('categories','authors'));
    }

    /**
     * Store a newly created Art in storage.
     *
     * @param CreateArtRequest $request
     *
     * @return Response
     */
    public function store(CreateArtRequest $request)
    {
        $input = $request->all();
        $input['title'] = array(
            'uz'=>$request->title_uz,
            'ru'=>$request->title_ru,
            'en'=>$request->title_en,
        );
        $input['description'] = array(
            'uz'=>$request->description_uz,
            'ru'=>$request->description_ru,
            'en'=>$request->description_en,
        );
        $time = strtotime(date('Y-m-d H:i:s'));
        $fileType = $request->image->extension();
        $fileName = $time . '.' . $fileType;
        $request->image->move(public_path('files/uploaded_arts'), $fileName);

        $input['images'] = $fileName;

        $art = $this->artRepository->create($input);

        Flash::success('Art saved successfully.');

        return redirect(route('art.index'));
    }

    /**
     * Display the specified Art.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $art = $this->artRepository->find($id);

        if (empty($art)) {
            Flash::error('Art not found');

            return redirect(route('art.index'));
        }

        return view('art.show')->with('art', $art);
    }

    /**
     * Show the form for editing the specified Art.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $art = $this->artRepository->find($id);
        $categories = Category::all();
        $authors = Author::all();

        if (empty($art)) {
            Flash::error('Art not found');

            return redirect(route('art.index'));
        }

        return view('art.edit',compact('categories','authors'))->with('art', $art);
    }

    /**
     * Update the specified Art in storage.
     *
     * @param  int              $id
     * @param UpdateArtRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArtRequest $request)
    {
        $art = $this->artRepository->find($id);
        $input = $request->all();
        $input['title'] = array(
            'uz'=>$request->title_uz,
            'ru'=>$request->title_ru,
            'en'=>$request->title_en,
        );
        $input['description'] = array(
            'uz'=>$request->description_uz,
            'ru'=>$request->description_ru,
            'en'=>$request->description_en,
        );

        if ($request->hasFile('image')) {
            $time = strtotime(date('Y-m-d H:i:s'));
            $fileType = $request->image->extension();
            $fileName = $time . '.' . $fileType;
            $request->image->move(public_path('files/uploaded_arts'), $fileName);
//            unlink(public_path('files/uploaded_arts').'/'. $art->image);
            $input['images'] = $fileName;
        }else{
            $input['images'] = $art->image;
        }

        $art = $this->artRepository->update($input, $id);

        Flash::success('Art updated successfully.');

        return redirect(route('art.index'));
    }

    /**
     * Remove the specified Art from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $art = $this->artRepository->find($id);

        if (empty($art)) {
            Flash::error('Art not found');

            return redirect(route('art.index'));
        }

        $this->artRepository->delete($id);

        Flash::success('Art deleted successfully.');

        return redirect(route('art.index'));
    }
}

<?php

namespace App\Http\Controllers;

use App\DataTables\AuthorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAuthorRequest;
use App\Http\Requests\CreateTeamRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Repositories\AuthorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AuthorController extends AppBaseController
{
    /** @var  AuthorRepository */
    private $authorRepository;

    public function __construct(AuthorRepository $authorRepo)
    {
        $this->authorRepository = $authorRepo;
    }

    /**
     * Display a listing of the Author.
     *
     * @param AuthorDataTable $authorDataTable
     * @return Response
     */
    public function index(AuthorDataTable $authorDataTable)
    {
        return $authorDataTable->render('authors.index');
    }

    /**
     * Show the form for creating a new Author.
     *
     * @return Response
     */
    public function create()
    {
        return view('authors.create');
    }

    /**
     * Store a newly created Author in storage.
     *
     * @param CreateAuthorRequest $request
     *
     * @return Response
     */
    public function store(CreateAuthorRequest $request)
    {
        $input = $request->all();
        $time = strtotime(date('Y-m-d H:i:s'));
        $fileType = $request->image->extension();
        $fileName = $time . '.' . $fileType;
        $request->image->move(public_path('files/uploaded_author'), $fileName);

        $input['image'] = $fileName;

        $team = $this->authorRepository->create($input);

        Flash::success('Author saved successfully.');
        return redirect(route('teams.index'));
    }

    /**
     * Display the specified Author.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $author = $this->authorRepository->find($id);

        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }

        return view('authors.show')->with('author', $author);
    }

    /**
     * Show the form for editing the specified Author.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $author = $this->authorRepository->find($id);

        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }

        return view('authors.edit')->with('author', $author);
    }

    /**
     * Update the specified Author in storage.
     *
     * @param  int              $id
     * @param UpdateAuthorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAuthorRequest $request)
    {
        $author = $this->authorRepository->find($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $time = strtotime(date('Y-m-d H:i:s'));
            $fileType = $request->image->extension();
            $fileName = $time . '.' . $fileType;
            $request->image->move(public_path('files/uploaded_author'), $fileName);
            unlink(public_path('files/uploaded_author').'/'.$author->image);
            $input['image'] = $fileName;
        }else{
            $input['image'] = $author->image;
        }

        $author = $this->authorRepository->update($request->all(), $id);

        Flash::success('Author updated successfully.');

        return redirect(route('authors.index'));
    }

    /**
     * Remove the specified Author from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $author = $this->authorRepository->find($id);

        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }

        $this->authorRepository->delete($id);

        Flash::success('Author deleted successfully.');

        return redirect(route('authors.index'));
    }
}

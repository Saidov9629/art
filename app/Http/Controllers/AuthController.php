<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function checklogin(Request $request)
    {

        $request->validate([
            'email'     => 'required|string',
            'password'  => 'required|string',
        ]);

        $user = User::query()->where('email',$request->email)->first();

        if ($user!=null && Hash::check($request->password, $user->password)) {
            Auth::login($user);
            return redirect('dashbord');
        }
        return redirect(route('login'))->with('error', 'Oppes! You have entered invalid credentials');;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}

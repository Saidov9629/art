<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTeam_memberAPIRequest;
use App\Http\Requests\API\UpdateTeam_memberAPIRequest;
use App\Models\Team_member;
use App\Repositories\Team_memberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\Team_memberResource;
use Response;

/**
 * Class Team_memberController
 * @package App\Http\Controllers\API
 */

class Team_memberAPIController extends AppBaseController
{
    /** @var  Team_memberRepository */
    private $teamMemberRepository;

    public function __construct(Team_memberRepository $teamMemberRepo)
    {
        $this->teamMemberRepository = $teamMemberRepo;
    }

    /**
     * Display a listing of the Team_member.
     * GET|HEAD /teamMembers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $teamMembers = $this->teamMemberRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(Team_memberResource::collection($teamMembers), 'Team Members retrieved successfully');
    }

    /**
     * Store a newly created Team_member in storage.
     * POST /teamMembers
     *
     * @param CreateTeam_memberAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTeam_memberAPIRequest $request)
    {
        $input = $request->all();

        $teamMember = $this->teamMemberRepository->create($input);

        return $this->sendResponse(new Team_memberResource($teamMember), 'Team Member saved successfully');
    }

    /**
     * Display the specified Team_member.
     * GET|HEAD /teamMembers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Team_member $teamMember */
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            return $this->sendError('Team Member not found');
        }

        return $this->sendResponse(new Team_memberResource($teamMember), 'Team Member retrieved successfully');
    }

    /**
     * Update the specified Team_member in storage.
     * PUT/PATCH /teamMembers/{id}
     *
     * @param int $id
     * @param UpdateTeam_memberAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeam_memberAPIRequest $request)
    {
        $input = $request->all();

        /** @var Team_member $teamMember */
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            return $this->sendError('Team Member not found');
        }

        $teamMember = $this->teamMemberRepository->update($input, $id);

        return $this->sendResponse(new Team_memberResource($teamMember), 'Team_member updated successfully');
    }

    /**
     * Remove the specified Team_member from storage.
     * DELETE /teamMembers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Team_member $teamMember */
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            return $this->sendError('Team Member not found');
        }

        $teamMember->delete();

        return $this->sendSuccess('Team Member deleted successfully');
    }
}

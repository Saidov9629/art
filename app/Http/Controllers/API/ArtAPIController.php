<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateArtAPIRequest;
use App\Http\Requests\API\UpdateArtAPIRequest;
use App\Models\Art;
use App\Repositories\ArtRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ArtResource;
use Response;

/**
 * Class ArtController
 * @package App\Http\Controllers\API
 */

class ArtAPIController extends AppBaseController
{
    /** @var  ArtRepository */
    private $artRepository;

    public function __construct(ArtRepository $artRepo)
    {
        $this->artRepository = $artRepo;
    }

    /**
     * Display a listing of the Art.
     * GET|HEAD /art
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $art = $this->artRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(ArtResource::collection($art), 'Art retrieved successfully');
    }

    /**
     * Store a newly created Art in storage.
     * POST /art
     *
     * @param CreateArtAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateArtAPIRequest $request)
    {
        $input = $request->all();

        $art = $this->artRepository->create($input);

        return $this->sendResponse(new ArtResource($art), 'Art saved successfully');
    }

    /**
     * Display the specified Art.
     * GET|HEAD /art/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Art $art */
        $art = $this->artRepository->find($id);

        if (empty($art)) {
            return $this->sendError('Art not found');
        }

        return $this->sendResponse(new ArtResource($art), 'Art retrieved successfully');
    }

    /**
     * Update the specified Art in storage.
     * PUT/PATCH /art/{id}
     *
     * @param int $id
     * @param UpdateArtAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArtAPIRequest $request)
    {
        $input = $request->all();

        /** @var Art $art */
        $art = $this->artRepository->find($id);

        if (empty($art)) {
            return $this->sendError('Art not found');
        }

        $art = $this->artRepository->update($input, $id);

        return $this->sendResponse(new ArtResource($art), 'Art updated successfully');
    }

    /**
     * Remove the specified Art from storage.
     * DELETE /art/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Art $art */
        $art = $this->artRepository->find($id);

        if (empty($art)) {
            return $this->sendError('Art not found');
        }

        $art->delete();

        return $this->sendSuccess('Art deleted successfully');
    }
}

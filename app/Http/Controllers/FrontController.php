<?php

namespace App\Http\Controllers;

use App\Models\Art;
use App\Models\Team;
use App\Models\Team_member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FrontController extends Controller
{
    public function gallery(){
        $language = App::GetLocale();
        $team_members = Team_member::all();
        $arts = Art::all();
        $teams = Team::all();
        return view('gallery', ['language'=>$language,'team_members'=>$team_members,'arts'=>$arts, 'teams'=>$teams]);
    }
}

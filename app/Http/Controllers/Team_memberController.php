<?php

namespace App\Http\Controllers;

use App\DataTables\Team_memberDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTeam_memberRequest;
use App\Http\Requests\UpdateTeam_memberRequest;
use App\Models\Team;
use App\Repositories\Team_memberRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Team_memberController extends AppBaseController
{
    /** @var  Team_memberRepository */
    private $teamMemberRepository;

    public function __construct(Team_memberRepository $teamMemberRepo)
    {
        $this->teamMemberRepository = $teamMemberRepo;
    }

    /**
     * Display a listing of the Team_member.
     *
     * @param Team_memberDataTable $teamMemberDataTable
     * @return Response
     */
    public function index(Team_memberDataTable $teamMemberDataTable)
    {
        return $teamMemberDataTable->render('team_members.index');
    }

    /**
     * Show the form for creating a new Team_member.
     *
     * @return Response
     */
    public function create()
    {
        $teams = Team::all();

        return view('team_members.create', compact('teams'));
    }

    /**
     * Store a newly created Team_member in storage.
     *
     * @param CreateTeam_memberRequest $request
     *
     * @return Response
     */
    public function store(CreateTeam_memberRequest $request)
    {
        $input = $request->all();

        $teamMember = $this->teamMemberRepository->create($input);

        Flash::success('Team Member saved successfully.');

        return redirect(route('teamMembers.index'));
    }

    /**
     * Display the specified Team_member.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            Flash::error('Team Member not found');

            return redirect(route('teamMembers.index'));
        }

        return view('team_members.show')->with('teamMember', $teamMember);
    }

    /**
     * Show the form for editing the specified Team_member.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            Flash::error('Team Member not found');

            return redirect(route('teamMembers.index'));
        }

        return view('team_members.edit')->with('teamMember', $teamMember);
    }

    /**
     * Update the specified Team_member in storage.
     *
     * @param  int              $id
     * @param UpdateTeam_memberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeam_memberRequest $request)
    {
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            Flash::error('Team Member not found');

            return redirect(route('teamMembers.index'));
        }

        $teamMember = $this->teamMemberRepository->update($request->all(), $id);

        Flash::success('Team Member updated successfully.');

        return redirect(route('teamMembers.index'));
    }

    /**
     * Remove the specified Team_member from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $teamMember = $this->teamMemberRepository->find($id);

        if (empty($teamMember)) {
            Flash::error('Team Member not found');

            return redirect(route('teamMembers.index'));
        }

        $this->teamMemberRepository->delete($id);

        Flash::success('Team Member deleted successfully.');

        return redirect(route('teamMembers.index'));
    }
}

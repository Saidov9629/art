<?php

namespace App\Repositories;

use App\Models\Art;
use App\Repositories\BaseRepository;

/**
 * Class ArtRepository
 * @package App\Repositories
 * @version April 27, 2021, 2:09 pm UTC
*/

class ArtRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category_id',
        'title',
        'description',
        'images',
        'tags',
        'author_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Art::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Team_member;
use App\Repositories\BaseRepository;

/**
 * Class Team_memberRepository
 * @package App\Repositories
 * @version April 23, 2021, 7:16 pm UTC
*/

class Team_memberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'team_id',
        'pasition',
        'full_name',
        'info'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Team_member::class;
    }
}

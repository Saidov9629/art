<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class Art
 * @package App\Models
 * @version April 27, 2021, 2:09 pm UTC
 *
 * @property string $category_id
 * @property string $title
 * @property string $description
 * @property string $images
 * @property string $tags
 * @property string $author_id
 */
class Art extends Model
{


    public $table = 'arts';

    public function art_category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id' );
    }


    public $fillable = [
        'category_id',
        'title',
        'description',
        'images',
        'tags',
        'author_id'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'string',
        'title' => 'array',
        'description' => 'array',
        'images' => 'string',
        'tags' => 'string',
        'author_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];


}

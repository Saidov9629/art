<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class Category
 * @package App\Models
 * @version April 27, 2021, 12:07 pm UTC
 *
 * @property string $name
 * @property string $image
 */
class Category extends Model
{


    public $table = 'categories';


    public $fillable = [
        'name',
        'image',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'array',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];


}

<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class Team
 * @package App\Models
 * @version April 23, 2021, 6:31 pm UTC
 *
 * @property string $image
 * @property string $name
 * @property string $description
 */
class Team extends Model
{


    public $table = 'teams';
    



    public $fillable = [
        'image',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'image' => 'required',
        'name' => 'required',
        'description' => 'required'
    ];

    
}

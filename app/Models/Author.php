<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class Author
 * @package App\Models
 * @version April 27, 2021, 8:51 am UTC
 *
 * @property string $image
 * @property string $full_name
 * @property string $info
 */
class Author extends Model
{


    public $table = 'authors';
    



    public $fillable = [
        'image',
        'full_name',
        'info'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'full_name' => 'string',
        'info' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'full_name' => 'required',
        'info' => 'required'
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class Team_member
 * @package App\Models
 * @version April 23, 2021, 7:16 pm UTC
 *
 * @property string $team_id
 * @property string $pasition
 * @property string $full_name
 * @property string $info
 */
class Team_member extends Model
{


    public $table = 'team_members';
    



    public $fillable = [
        'team_id',
        'pasition',
        'full_name',
        'info'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'team_id' => 'string',
        'pasition' => 'string',
        'full_name' => 'string',
        'info' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}

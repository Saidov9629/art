<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class User
 * @package App\Models
 * @version April 23, 2021, 5:25 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $password
 */
class User extends Model
{


    public $table = 'users';




    public $fillable = [
        'name',
        'email',
        'password'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required'
    ];


}
